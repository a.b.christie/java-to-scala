# JavaToScala

A journey from Java to Scala (a presentation).

The project's file `presentation.md` is a [markdown] document formatted for
[mdp] - a command-line based markdown presentation tool. The slide content
is written assuming a standard terminal size of 80 x 24 characters.

- Tested with mdp v1.0.7

---

[markdown]: http://daringfireball.net/projects/markdown/
[mdp]:      https://github.com/visit1985/mdp
