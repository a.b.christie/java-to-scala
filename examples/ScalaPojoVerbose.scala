package com.sysmech.simpulse.experiment

/**
  * Note that the three getters (each starting with the def keyword) are
  * grouped together in the middle and the three setters at the end,
  * to help make the difference clear.
  * <p/>
  * The getters have the same names that we had originally used for fields,
  * so the private fields themselves have slightly altered names -
  * they are not allowed to have the same name.
  * <p/>
  * Because the getters have supplanted the original fields, any code elsewhere
  * that uses this class doesn't need any alteration:
  * <p/>
  * This is the principle of _uniform access_ put into practice.
  *
  * @author Alan Christie
  */
class ScalaPojoVerbose {
    
    private[this] var mComponentName: String = _
    private[this] var mIdentity: Int = _
    private[this] var mReady: Boolean = _
    
    // Getters
    
    def componentName: String = {
        mComponentName
    }
    
    def identity: Int = {
        mIdentity
    }
    
    def ready: Boolean = {
        mReady
    }
    
    // Setters
    
    def componentName_= (s: String) = {
        mComponentName = s
    }
    
    def identity_= (a: Int) = {
        mIdentity = a
    }
    
    def ready_= (m: Boolean) = {
        mReady = m
    }

}
