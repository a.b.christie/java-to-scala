package com.sysmech.simpulse.experiment

import net.jcip.annotations.NotThreadSafe

/**
  * A basic 'plain old java object' with 3 members.
  * <p/>
  * Everything is public in Scala unless marked `protected` or `private`.
  * <p/>
  * It looks like the Scala code above breaks the encapsulation convention:
  * there are three fields, all variable and publicly accessible. In Java,
  * we don't like having public fields because we lose the option to change
  * them later and use setters/getters instead (as discussed above).
  * But in Scala, this is not a problem.
  * <p/>
  * This works because Scala has a _uniform access principle_:
  * a public variable can be replaced with a getter of the same name and a
  * corresponding setter. Conversely, a getter/setter pair with simple
  * behaviour can be replaced with an equivalent public variable.
  * Scala guarantees that the two forms are equivalent.
  * <p/>
  * So if Scala allows public fields to be converted to getters/setters
  * (and vice versa) painlessly, it can painlessly abandon Java's convention
  * on encapsulation. Therefore the pojo is reduced to the three fields alone.
  * <p/>
  * If we really wanted, we could change the three fields in ScalaPojo
  * to be private in the JavaBean style. There would be no benefit until the
  * methods needed extra behaviour, but to illustrate how this is done,
  * consider the verbose version.
  *
  * @author Alan Christie
  */
@NotThreadSafe
class ScalaPojo {
    
    var componentName: String = _
    var identity: Int = _
    var ready: Boolean = _
    
}
