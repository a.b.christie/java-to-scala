package com.sysmech.simpulse.experiment

/**
  * Actually the `ScalaPojoVerbose` is unusually verbose for Scala,
  * which has strong conventions for eliminating unnecessary fluff and clutter.
  * <p/>
  * So below there's a third Scala version, ScalaPojoSlim, also with getters
  * and setters but with minimal stuff in the way. Note that the getters have
  * been slimmed down, not needing return type nor braces.
  * <p/>
  * The setters are shown as one-liners, which is what they are in this case.
  * They don't strictly need the braces either, but they would be harder
  * to read without them and its contrary to most coding styles.
  *
  * @author Alan Christie
  */
class ScalaPojoSlim {
    
    private[this] var mComponentName: String = _
    private[this] var mIdentity: Int = _
    private[this] var mReady: Boolean = _
    
    // Getters
    
    def componentName = mComponentName
    def identity =  mIdentity
    def ready =  mReady
    
    // Setters
    
    def componentName_= (s: String) = { mComponentName = s }
    def identity_= (a: Int) = { mIdentity = a }
    def ready_= (m: Boolean) = { mReady = m }

}
