package com.sysmech.simpulse.experiment;

import net.jcip.annotations.NotThreadSafe;

/**
 * A 'plain old java object' with three members.
 * <p/>
 * There are only three fields, but look how much code is needed.
 * <p/>
 * I would normally tend to add a lot more documentation but this class
 * is used to illustrate the differences between route Java and Scala
 * have taken.
 *
 * @author Alan Christie
 */
@NotThreadSafe
public class JavaPojo {

    private String componentName;
    private int identity;
    private boolean ready;

    public String getComponentName() {
        return componentName;
    }

    public void setName(final String componentName) {
        this.componentName = componentName;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(final int identiy) {
        this.identity = identity;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(final boolean ready) {
        this.ready = ready;
    }

}
