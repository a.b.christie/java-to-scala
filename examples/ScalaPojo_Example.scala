package com.sysmech.simpulse.experiment

/**
  * WRITE ME!
  * <p/>
  * This class is not thread safe.
  * <p/>
  * Copyright (C) 2016 Systems Mechanics Ltd. All rights reserved.
  *
  * @author Alan Christie
  */
object ScalaPojo_Example {
    
    var pojoA: ScalaPojo = new ScalaPojo
    var pojoB: ScalaPojoVerbose = new ScalaPojoVerbose
    var pojoC: ScalaPojoSlim = new ScalaPojoSlim
    
    def main(args: Array[String]): Unit = {
        
        pojoA.componentName = "Bolt (Simple)"
        pojoB.componentName = "Bolt (Verbose)"
        pojoC.componentName = "Bolt (Slim)"
        
        println(pojoA.componentName)
        println(pojoB.componentName)
        println(pojoC.componentName)
        
    }
    
}
