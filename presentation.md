%title: Experiences moving from Java to Scala
%date: 2016-08-10

-> A brief presentation on a journey from Java to Scala. <-

^
## About me...
Software developer with a passion for...
- Test driven design
- High performance real-time embedded solutions
- Simplification, continuous integration & delivery

^
## Professional career...
- *2006 - Today* Event Correlation _Zenulta/Systems Mechanics_
  - Java, Python, Ruby
- *1999 - 2006* GSM/UMTS Firmware _Lucent_
  - Assembler, C, C++, Python
- *1990 - 1999* Speech Recognition _Dragon Systems_
  - Assembler (DSP, MMX), Pascal, C, C++, Java
- *1987 - 1990* Radio Engineer _BBC_
  - Hardware

^
But ... not much experience with *Scala*!

---

## Why move from Java?

Current project has *too many lines of code*. 85,000 lines over 1,600 classes.
Lines that...
- need documenting
- need reading
- need testing
- need maintaining
- lead to bugs

^
Also more and more frustrated by Java's *lack of 'modern' constructs*...
- Default/named arguments
- Immutability
- A thread model based on shared memory and locking

^
And...
- I'm just writing too much 'boilerplate' code
- Sliding across to *Scala* appears straightforward
- Isn't 10 years of any one language enough for anyone?

---

## It's mainly about size (and simplicity)

Reduction in code size results in increased productivity.
This is true if you believe that a developer's productivity is based on 
lines of code per day, independent of language (which I do).

A lot fo Scala's criticism is based around the approach it's taken.
It can appear complex.
- *Complex* code can be simple
- *Complicated* code cannot

^
> If you can write a slightly more complex single line of code that
> replaces 20 "simpler" lines of Java, which one is really more complex?
>
> [Matt Hicks](https://www.toptal.com/scala/why-should-i-learn-scala)

---

## It's also about readability

Fundamentally - readability counts.

> The ratio of time spent reading versus writing is well over 10 to 1.
> Making it easy to read makes it easier to write.
>
> _[Robert C. Martin](http://www.goodreads.com/book/show/3735293-clean-code)_

^
In his paper, [Gilles Dubochet](https://infoscience.epfl.ch/record/138586/files/dubochet2009coco.pdf), describes how he investigated two aspects
of programming style using eye movement tracking. He found that it is,
on average, _30% faster to comprehend algorithms_ that use for-comprehensions
and maps, as in Scala, rather than those with the iterative while-loops
of Java.

---

## But it has to be fast, right?

^
That depends. Here's some sagely advice on performance...

^
> Avoid Premature Optimisation - Don’t even think about optimisation unless
> your code is working, but slower than you want. Only then should you start
> thinking about optimising, and then only with the aid of empirical data.
> "We should forget about small efficiencies, say about 97% of the time:
> premature optimisation is the root of all evil"
>
> _Donald Knuth_

^
> The fastest algorithm can frequently be replaced
> by one that is almost as fast and much easier to understand.
> 
> _D. Jones_

^
> Do the simplest thing that could possibly work
>
> _Ward Cunningham_

---

## Ultimately, we have to be productive

^
> It takes 3x the effort to find and fix bugs in system test
> than when done by the developer.
> It takes 10x the effort to find and fix ​bugs in the field
> than when done in system test.
>
> _Larry Bernstein - Bell Communications Research_

^
> A 2% reduction in defects is usually accompanied by a
> 10% increase in productivity.
>
> _Lynas - Harvard Business Review, August, 1981_

---

## Why Scala?

Scala's a *Sca*\lable *La*\nguage. It is designed to be extended and adapted.
`BigInt` feels like a built-in type in *Scala* but it isn't, it's just a class.

- Some languages attempt to provide everything but this doesn't scale well.
- Instead Scala allows users to grow the language in the direction they want.
- Scala fuses _functional_ and _imperative_ programming.
- None of this, of course, may be of importance to you.

>	Scala typically takes _one half to one third of the lines of code_ to
>	express the same application as Java yet executes at about the same speed.

>	Scala better matches _the way programmers think_.
>   (See previous slide)

---

## Scala is obvious (for Java)

- It can _interoperate with Java_
  - It can call into Java
  - Java can call into it
  - It compiles to Java byte code
  - It runs in the same JVM
- It's _designed to be a better language_
- Uses the _uniform access principle_ (more on this later)
- There's an _interactive console_ for experimentation

^
Scala's got a learning curve. It's a rather different approach but this
allows it to offer some interesting advances. It may be difficult at first
but if this puts you off maybe you're in the wrong business?

^
There's been [Kotlin](http://kotlinlang.org) and [Ceylon](http://ceylon-lang.org) but they chose to remain very close
in syntax to Java to avoid a learning curve but they force you to stay
with a the language issues that may be irritating you already!

---

## What have I been looking at?

- Enumerations
- POJOs (structures)
- Concurrency
- Handling null
- Iterations
- Default arguments

---

## Enumerations

For me they're just type-safe equivalents of *C/C++* constants.

In *Java*...

    enum Instruction {
        ON,
        OFF
    }

In *Scala*...

    object Instruction extends Enumeration {
        type Instruction = Value
        val ON,
        OFF = Value
    } 

Sadly, Java's enum is just easier to read (and therefore understand).

---

## POJOs (structures)

In *Java*...

-> See <-
-> _JavaPojo_ <-
    
In *Scala*...

-> See <-
-> _ScalaPojo_ <-
-> _ScalaPojoVerbose_ <-
-> _ScalaPojoSlim_ <-

From [Easy Pojos in Scala](http://www.bigbeeconsultants.co.uk/blog/easy-pojos-scala)

---

## Concurrency

Scala's thread model promotes the use of `Actors`.

- Message based
  - Inspiration drawn from *Erlang*
- Send to a mailbox
  - `recipient \! msg`
- Receiver is just a series of case statements

    def receive = {
        case Msg1 => ... // Handle Msg1
        case Msg2 => ... // Handle Msg2
        // ...
    }

^
In Java communicating between threads requires you to roll-your-own code

---

## Handling null

A significant advantage of *Scala* is the that the use of `null`
is strongly discouraged in favour of `None` where the behaviour is obvious

In *Java* if method can return `null` at the very least you need to
- Read their JavaDoc
  - ...and hope they've documented the behaviour!
- Write code to handle it

^
In *Scala* you know a function behaves like this because you see `Option[Int]`

^
    def toInt(in: String): Option[Int] = {
        try {
            Some(Integer.parseInt(in.trim))
        } catch {
            case e: NumberFormatException => None
        }
    }

---

## Handling null (continued...)

The *Java* receiver...

	Integer i = toInt(someString);
	if (i == null) {
		System.out.println("That didn't work.");
	} else {
		System.out.println(i);
	}

The *Scala* receiver...

	toInt(someString) match {
		case Some(i) => println(i)
		case None => println("That didn't work.")
	}

^
There's really not much difference. Except you there's no unexpected exception!
^
You'll still encounter the `NullPointerException` - but that's another topic.

---

## Iterations

Here's where Scala can offer some significant improvement on readability.

Example: convert list of strings to numbers

^
In *java*...

    List<Integer> ints = new ArrayList<Integer>();
    for (String s : list) {
        ints.add(Integer.parseInt(s));
    }

^
In *Scala*...

    val ints = list.map(s => s.toInt)

---

## Iterations (continued...)

Example: Collecting products of a particular category

^
In *Java*...

    List<Product> products = new ArrayList<Product>();
    for (Order order : orders) {
        for (Product product : order.getProducts()) {
            if (category.equals(product.getCategory())) {
                products.add(product);
            }
        }
    }
    return products;

^
In *Scala*...

    orders.flatMap(o => o.products).filter(p => p.category == category)

---

## Is Scala really more complex?

Process objects (a `Packet`) based on its content...

*Java 7*

    for (Packet packet : packets) {
        if (packet.ordinal > 0) {

*Java 8*

    packets.stream().
        filter(packet -> packet.ordinal > 0).
            forEach(packet -> {

*Scala*

    packets.filter(_.ordinal > 0).foreach {

---

## Performance?

Using a classic Bubble sort algorithm tests have been performed on
a list of 100,000 elements 100 times (times based on a 2012 MacBook Pro).

- Java *825mS*
- Scala (imperative) *695mS* [20% faster]
- Scala (idiomatic) *13,951mS* [1,600% slower]

^
## But the idiomatic code looks like this...

    def sortList(list: List[Int]): List[Int] = list match {
        case Nil => Nil
        case head :: tail => sortList(tail.filter(_ < head)) :::
             head :: sortList(tail.filter(_ >= head))
    }

That's 23 lines of classic bubble-sort code down to 4 (crazy) lines.

-> Compare with the Java code <-

---

## One study

In a comparison between C++, Java, Go and Scala [Robert Hundt](https://days2011.scala-lang.org/sites/days2011/files/ws3-1-Hundt.pdf)
compared language implementations using the languages’ idiomatic container
classes, looping constructs, and memory/object allocation schemes but did
not attempt to exploit specific language and run-time features
to achieve maximum performance.

Code size (lines of code)
- Java is *_1.6x larger_* than Scala

Memory footprint
- Java is *_2.1x larger_* than Scala

Performance
- Java is *_1.6x slower_* than Scala

---

## Contra?

- _No statics_. It's all objects in *Scala* so you use the singleton pattern.
- _Idiomatic_. The more powerful aspects of the language can be mind-bending.
- _Editors_. Easy for basics, but refactoring doesn't work well.
- _Tools_. Are supporting tools (see next slide).

---

## There are important peripheral tools

A suite of development tools doesn't stop at the compiler.
You need static analysers, coverage, profiling and mocking frameworks.
- [FindBugs](http://findbugs.sourceforge.net)
- [Cobertura](http://cobertura.github.io/cobertura/)
- [JProfiler](https://www.ej-technologies.com/products/jprofiler/overview.html)
  - or VisaulVM, Mission Control etc.
- [Mockito](http://mockito.org)

^
And we haven't looked at...
- *Traits*
- *Default parameters* (in detail)
- *Memory usage*
- *Performance*

